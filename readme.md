
# Create database
CREATE DATABASE IF NOT EXISTS rest_interview_test
    DEFAULT CHARACTER SET utf8
    DEFAULT COLLATE utf8_unicode_ci
;

# Run composer
composer install --prefer-dist

# Run migration with seeders
artisan migrate:install
artisan migrate --seed

# Create new host on homestead (0.3.3) box
serve-laravel rest-interview-test.app /full/path/to/project-folder/public

# Edit hosts file
192.168.10.10   rest-interview-test.app

# Use this username/credential to login
milan/milan
vagrant/vagrant

# List all routes
php artisan route:list
