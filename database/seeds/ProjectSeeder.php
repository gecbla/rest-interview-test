<?php

use Illuminate\Database\Seeder;

class ProjectSeeder extends Seeder
{

    public $index = 1;

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $users = [ 'milan', 'vagrant' ];

        foreach ($users as $user) {
            $user = \App\User::create([
                'email'=> $user . '@gmail.com',
                'username' => $user,
                'password' => bcrypt($user),
            ]);

            if ($user) {
                $productsPerUser = rand(6, 11);

                foreach (range(1, $productsPerUser) as $key) {
                    $article = new \App\Article([
                        'article_name' => 'Article #' . $this->index,
                        'article_description' => 'Description #' . $this->index,
                        'article_price' => rand(22, 88) * 100,
                    ]);

                    $user->articles()->save($article);

                    $this->index++;
                }
            }
        }
    }
}
