
$(document).ready(function() {

    // index logic
    if ($('body.article-index').length) {
        callIndexEndpoint();

        // attach events
        $.each([ 'select[name=user_id]', 'select[name=page]' ], function(index, selector) {
            $(selector).change(function () {
                callIndexEndpoint();
            });
        });
    } // end index

    // create logic
    if ($('body.article-create').length) {

        $('button.btn-default').click(function (event) {
            event.preventDefault();
            event.stopImmediatePropagation();

            var createEndpoint = '/api/articles';

            if (!isValidCreateInput()) {
                $('#message').text('Please enter valid data!');
                return;
            }

            jQuery.ajax({
                url: createEndpoint,
                type: 'POST',
                dataType: 'json',
                contentType: 'application/json; charset=utf-8',
                data: JSON.stringify({
                    "user_id": parseInt($('input[name=user_id]').val(), 10),
                    "article_name": $('input[name=article_name]').val(),
                    "article_description": $('input[name=article_description]').val(),
                    "article_price": parseInt($('input[name=article_price]').val(), 10),
                }),
                success: function(response, textStatus, jqXHR) {
                    if (response.status == 'success') {
                        $('#message').text(response.message);
                        $('form:first').get(0).reset();
                    }
                }
            });
        });
    } // end create

    // update logic
    if ($('body.article-update').length) {

        $('button.btn-default').click(function (event) {
            event.preventDefault();
            event.stopImmediatePropagation();

            var id = parseInt($('input[name=article_id]').val(), 10);
            var updateEndpoint = '/api/articles/' + id;

            jQuery.ajax({
                url: updateEndpoint,
                type: 'PUT',
                dataType: 'json',
                contentType: 'application/json; charset=utf-8',
                data: JSON.stringify({
                    "article_id": id,
                    "article_name": $('input[name=article_name]').val(),
                    "article_description": $('input[name=article_description]').val(),
                    "article_price": parseInt($('input[name=article_price]').val(), 10),
                }),
                success: function(response, textStatus, jqXHR) {
                    $('#message').text(response.message);
                }
            });
        });
    } // end update

    // delete logic
    if ($('body.article-delete').length) {

        var $input = $('input[name=article_id]');
        $input.focus();

        $('button.btn-default').click(function (event) {
            event.preventDefault();
            event.stopImmediatePropagation();

            if ($input.val() != '') {

                var id = parseInt($input.val(), 10);
                var deleteEndpoint = '/api/articles/' + id;

                jQuery.ajax({
                    url: deleteEndpoint,
                    type: 'DELETE',
                    dataType: 'json',
                    success: function(response, textStatus, jqXHR) {
                        $('#message').text(response.message);
                    }
                });
            };
        });
    } // end delete

});

function callIndexEndpoint() {
    var indexEndpoint = getIndexEndpoint();

    jQuery.ajax({
        url: indexEndpoint,
        type: 'GET',
        success: function(articles, textStatus, jqXHR) {

            var $dataContainer = $('tbody.grid-data');
                rows = [];

            if (!articles.length) {
                $dataContainer.empty().append('<tr><td colspan="8">Sorry no data!</tr>');
                return;
            }

            $.each(articles, function(index, article) {
                rows.push(buildRow(article));
            });

            $dataContainer.empty().append(rows.join(''));
        }
    });
}

function buildRow(article) {
    var rows = [];

    rows[0] = "<td>" + article.id + "</td>";
    rows[1] = "<td>" + article.user_id + "</td>";
    rows[2] = "<td>" + article.article_name + "</td>";
    rows[3] = "<td>" + article.article_description + "</td>";
    rows[4] = "<td>" + article.article_price + "</td>";
    rows[5] = "<td>" + article.created_at + "</td>";
    rows[6] = "<td>" + article.updated_at + "</td>";
    rows[7] = "<td>" + buildHelper(article.id) + "</td>";

    return '<tr>' + rows.join('') + '</tr>';

}

function buildHelper(article_id) {
    var rows = [];

    rows[0] = '<a href="/articles/update/' + article_id  +'">Edit</a>';
    rows[1] = '<a href="/articles/delete?id=' + article_id + '">Delete</a>';

    return rows.join('&nbsp;&nbsp;&nbsp;');
}

function getIndexEndpoint() {
    var indexEndpoint = '/api/articles',
        filters = [];

    if ($('select[name=user_id]').val() != 0) {
        filters.push('user_id=' + $('select[name=user_id]').val());
    }

    if ($('select[name=page]').val() != 0) {
        filters.push('page=' + $('select[name=page]').val());
    }

    indexEndpoint += (filters.length) ? '?' + filters.join('&') : '';

    return indexEndpoint;
}

function isValidCreateInput() {
    var isValid = true;

    if ($('input[name=article_name]').val() == '') {
        isValid = false;
    }

    if ($('input[name=article_description]').val() == '') {
        isValid = false;
    }

    if ($('input[name=article_price]').val() == '') {
        isValid = false;
    }

    return isValid;
}
