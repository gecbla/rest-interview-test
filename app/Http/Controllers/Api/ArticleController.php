<?php

namespace App\Http\Controllers\Api;

use App\Http\Requests;
use Illuminate\Http\Request;

class ArticleController extends BaseController
{

    // Task - DONE
    public function index(Request $request)
    {
        $itemsPerPage = 10;

        $articles = \App\Article::take($itemsPerPage)->orderBy('id', 'desc');

        if ($request->has('user_id')) {
            $user_id = (int) $request->query('user_id');

            if ($user_id >= 1) {
                $articles->where('user_id', $user_id);
            }
        }

        if ($request->has('page')) {
            $page = (int) $request->query('page', 1);

            if ($page >= 1) {
                $articles->skip(($page - 1) * $itemsPerPage);
            }
        }

        return $articles->get();
    }

    // Task - DONE
    public function create(Request $request)
    {
        /** @var \Symfony\Component\HttpFoundation\ParameterBag $payload */
        $payload = $request->json();

        $payloadArray = $payload->all();

        $createPayload = array_only($payloadArray, ['article_name', 'article_description', 'article_price']);

        $user_id = (int) $payloadArray['user_id'];

        $article = new \App\Article($createPayload);

        $user = \App\User::find($user_id);

        $isCreated = $user->articles()->save($article);

        if (!$isCreated) {
            return $this->respondWithError('Article couldn\'t be created!');
        }

        return $this->respondWithSuccess('Article was created!');
    }

    // Task! - DONE
    public function update(Request $request, $id)
    {
        $id = (int) $id;

        /** @var \Symfony\Component\HttpFoundation\ParameterBag $payload */
        $payload = $request->json();

        $filteredPayload = array_only($payload->all(), ['article_name', 'article_description', 'article_price']);

        $article = \App\Article::find($id);

        if (!$article) {
            return $this->respondWithError('Article with that ID doesn\'t exists and can\'be updated!');
        }

        $isUpdated = $article->update($filteredPayload);

        if (!$isUpdated) {
            return $this->respondWithError('Article couldn\'t be updated!');
        }

        return $this->respondWithSuccess('Article was updated!');
    }

    // Task - Done
    public function delete($id)
    {
        $id = (int) $id;

        $article = \App\Article::find($id);

        if (!$article) {
            return $this->respondWithError('Article with that ID doesn\'t exists or it\'s already deleted!');
        }

        $isDeleted = $article->delete();

        if (!$isDeleted) {
            return $this->respondWithError('Couldn\'t delete article!');
        }

        return $this->respondWithSuccess('Article was deleted!');
    }
}
