<?php

namespace App\Http\Controllers\Api;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class BaseController extends Controller
{

    public function respondWithError($message)
    {
        return response()->json([
            'status' => 'error',
            'message' => $message
        ]);
    }

    public function respondWithSuccess($message, $data = [])
    {
        return response()->json([
            'status'  => 'success',
            'message' => $message
        ]);
    }
}
