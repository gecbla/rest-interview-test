<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use Auth;

class ArticleController extends Controller
{
    public function index()
    {
        $itemsPerPage = 10;

        return view('articles.index', [
            'bodyClass' => 'article-index',
            'totalPages' => (int) ceil(\App\Article::count()/$itemsPerPage),
        ]);
    }

    public function create()
    {
        return view('articles.create', [
            'bodyClass' => 'article-create',
            'user' => Auth::user(),
        ]);
    }

    public function update($id)
    {
        $id = (int) $id;

        $article = \App\Article::find($id);

        return view('articles.update', [
            'bodyClass' => 'article-update',
            'article' => $article,
        ]);
    }

    public function delete(Request $request)
    {
        $id = (int) $request->query('id');

        return view('articles.delete', [
            'bodyClass' => 'article-delete',
            'id' => ($id >= 1) ? $id : '',
        ]);
    }
}
