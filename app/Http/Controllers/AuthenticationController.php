<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Auth;

class AuthenticationController extends Controller
{
    public function login()
    {
        if (Auth::check()) {
            return redirect()->route('article.index');
        }

        return view('auth.login');
    }

    public function check(Request $request)
    {
        $username = $request->input('username');
        $password = $request->input('password');

        if (!Auth::attempt(['username' => $username, 'password' => $password])) {
            return redirect()->route('login')
                             ->withInput($request->all())
                             ->withErrors([
                                 'username' => 'Entered credentials does not match our records!'
                             ]);
        }

        return redirect()->route('article.index');
    }

    public function logout()
    {
        if (Auth::check()) {
            Auth::logout();
        }

        return redirect()->route('login');
    }
}
