<?php

Route::get('/', function () {
    return view('welcome');
});

Route::get('login', [
    'uses' => 'AuthenticationController@login',
    'as'   => 'login',
]);

Route::post('login', [
    'uses' => 'AuthenticationController@check',
]);

Route::get('logout', [
    'uses' => 'AuthenticationController@logout',
    'as'   => 'logout',
]);

Route::group(['middleware' => 'auth'], function () {
    Route::get('articles', [
        'uses' => 'ArticleController@index',
        'as'   => 'article.index',
    ]);

    Route::get('articles/create', [
        'uses' => 'ArticleController@create',
        'as'   => 'article.create',
    ]);

    Route::get('articles/update/{id}', [
        'uses' => 'ArticleController@update',
        'as'   => 'article.update',
    ]);

    Route::get('articles/delete', [
        'uses' => 'ArticleController@delete',
        'as'   => 'article.delete',
    ]);
});

Route::group(['prefix' => 'api/articles', 'as' => 'api.article.', 'namespace' => 'Api'], function () {
    Route::get('/', [
        'uses' => 'ArticleController@index',
        'as'   => 'index'
    ]);

    Route::post('/', [
        'uses' => 'ArticleController@create',
        'as'   => 'create'
    ]);

    Route::put('{id}', [
        'uses' => 'ArticleController@update',
        'as'   => 'update'
    ]);

    Route::delete('{id}', [
        'uses' => 'ArticleController@delete',
        'as'   => 'delete'
    ]);
});
