
Test:

Specifikacije tehnologije:
PHP - koristices framework Laravel 5
HTML, CSS
JS/jQuery, Ajax
MYSQL

Koristices frontend framework Bootstrap.
Koristices REST API servis i rad sa JSON.

Potrebno je da napravis prostu aplikaciju koja ce imati korisnike koji mogu da se loguju gde ce posle uspesnog
login-a moci da kreiraju artikle.

+U MySQL bazi je potrebno da imas dve tabele USERS i ARTICLES koje su u relaciji, polja koja ce imati ti postavi
proizvoljno, neka budu to standardna polja, to jest osnovna.

Stranice koje su potrebne i mogucnosti:

 Login/Logout   => Tu ce korisnici da se loguju. Korisnik ne treba da se registuje on je vec zakucan u bazi u tabeli users.
*List Articles  => To je lista svih article-clanaka koji se nalaze u bazi. Na toj stranici moguce je da se izdvoje
				  samo artikli po korisniku ili da budu svi prikazani. Potrebno je uraditi odgovarajucu paginaciju!
 Single Article => To je stranica za prikaz artikla.
*Create Article => Stranica za kreiranje artikla. Tu moze samo ulogovani korisnik da kreira artikal.
 Update Article => Stranica za update article.
*Delete Article => Stranica za brisanje.

Jedna specificna stvar za sledece stranice:

List Articles , Create Article i Delete Article je ta sto te stranice ce raditi preko Ajax-a koji ce komunicirati
sa REST API-jem na backend strani.

1. Kod **List Articles** ces slati Ajax-om request ka REST apiju gde ce on da ti vraca JSON u kojem se nalazi
   niz objekata gde su smesteni podaci za articles.
2. Kod **Create Article** ces slati Ajax request ka REST apiju gde ce on da upise article u bazu i da vrati json
   poruku o uspesnosti.
3. Kod **Delete Article** ces slati Ajax request ka REST apiju gde ce on da izbrise article iz baze i da vrati json
   poruku o uspesnosti.

Svaki AJAX request salje JSON podatak i REST API vraca JSON podatak.

Pozdrav,
Milos Stojanovic
