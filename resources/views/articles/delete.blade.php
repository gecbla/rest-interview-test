@extends('layouts.main')

@section('content')
    <form class="form-horizontal">

        <div id="message"></div>

        <div class="form-group">
            <label for="inputArticleId" class="col-sm-2 control-label">Article ID</label>
            <div class="col-sm-3">
                <input type="text" name="article_id" value="{{ $id }}" class="form-control" id="inputArticleId">
            </div>
        </div>

        <div class="form-group">
            <div class="col-sm-offset-2 col-sm-10">
                <button type="text" class="btn btn-default">Delete</button>
            </div>
        </div>
    </form>
@endsection
