@extends('layouts.main')

@section('content')
    <form>
        <select name="user_id" id="filterByUser">
            <option value="0">Choose user</option>
            <option value="1">Milan</option>
            <option value="2">Vagrant</option>
        </select>
        <select name="page" id="filterByPage">
            <option value="0">Choose page</option>
            @foreach(range(1, $totalPages) as $page)
                <option value="{!! $page !!}">{!! $page !!}</option>
            @endforeach
        </select>
        <table class="grid">
            <thead>
                <tr>
                    <td>Article ID</td>
                    <td>User Id</td>
                    <td>Aricle name</td>
                    <td>Article desciption</td>
                    <td>Article price</td>
                    <td>Created at</td>
                    <td>Updated at</td>
                    <td>Action</td>
                </tr>
            </thead>
            <tbody class="grid-data">
            </tbody>
            <tfoot>
                <tr>
                    <td colspan="4"></td>
                    <td colspan="4"></td>
                </tr>
            </tfoot>
        </table>
    </form>
@endsection
