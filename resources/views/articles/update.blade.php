@extends('layouts.main')

@section('content')
    <form class="form-horizontal">

        <div id="message"></div>

        <input type="hidden" name="article_id" value="{{ $article->id }}" class="form-control" id="inputArticleId">

        <div class="form-group">
            <label for="inputArticleName" class="col-sm-2 control-label">Article name</label>
            <div class="col-sm-3">
                <input type="text" name="article_name" value="{{ $article->article_name }}" class="form-control" id="inputArticleName">
            </div>
        </div>

        <div class="form-group">
            <label for="inputArticleDescription" class="col-sm-2 control-label">Article desciption</label>
            <div class="col-sm-3">
                <input type="text" name="article_description" value="{{ $article->article_description }}" class="form-control" id="inputArticleDescription">
            </div>
        </div>

        <div class="form-group">
            <label for="inputArticlePrice" class="col-sm-2 control-label">Article price</label>
            <div class="col-sm-3">
                <input type="text" name="article_price" value="{{ $article->article_price }}" class="form-control" id="inputArticlePrice">
            </div>
        </div>


        <div class="form-group">
            <div class="col-sm-offset-2 col-sm-10">
                <button type="text" class="btn btn-default">Update</button>
            </div>
        </div>
    </form>
@endsection
