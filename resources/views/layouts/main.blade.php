<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Rest API</title>
    <link rel="stylesheet" href="/bootstrap/css/bootstrap.min.css">
    {{-- <script type="text/javascript" src="/js/jquery.min.js"></script> --}}
    <script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.js"></script>
    <script type="text/javascript" src="/js/articles.js"></script>
    <script type="text/javascript" src="/bootstrap/js/bootstrap.min.js"></script>
    <style>
        body {
            padding: 25px;
        }
    </style>
</head>
<body class="{{ $bodyClass or '' }}">

    <div class="container">
        @yield('content')
    </div>

</body>
</html>